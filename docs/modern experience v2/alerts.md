![alerts](../images/modernx/40.alerts.png)

Decide how you would like the web part messages to be displayed. Three options are available:
 
- **Only in edit mode** - Alert messages will be visible only if the page is in edit mode.
- **Always** - Alert messages will always be visible to any user.
- **Never** - Alerts messages will never be visible.