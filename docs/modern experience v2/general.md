![options](../images/modernx/04.property_options.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Documents Source](./datasource)
- [Layout](./layout)
- [Format](./format)
- [Social Engagement](./socialEngagement)
- [Actions](./actions)
- [Search/Filter/Sort](./searchFilterSort)
- [Theme](./theme)
- [Anchors](./anchors)
- [Audiences](./audiences)
- [Alerts](./alerts)