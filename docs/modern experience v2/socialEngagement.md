By allowing the following options, you will add diferent types of social engagement to the documents.

<p class="alert alert-warning">
These options need extra configuration outside of the webpart before being used.
Please, check the documentation provided <a href="https://support.bindtuning.com/hc/en-us/articles/13330848492561">here</a> before proceeding.
</p>

___
### Likes
This adds a like option to the Tiles layout and the document preview.

Likes on the Tiles layout:

![like_on_tile](../images/modernx/22.like_on_tiles.png)

Likes on the document preview:

![like_on_modal](../images/modernx/23.like_on_modal.png)

___
### Star Ratings
This adds a star rating option to the Tiles layout and the document preview.

Ratings on the Tiles layout:

![rating_on_tiles](../images/modernx/24.rating_on_tiles.png)

Ratings on the document preview:

![rating_on_modal](../images/modernx/25.rating_on_modal.png)

___
### Comments
This adds the ability to comment documents through the document preview.

![comments](../images/modernx/26.comments.png)
