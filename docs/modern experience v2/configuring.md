**Option 1**

1. Open the page where you want to add the web part and click on the **Edit** button;
    
   ![edit-page](../images/modernx/01.edit_modernx.png)

2. Mouse hover the web part and click on the pencil (✏️) icon that will appear ;
    ![configure](../images/modernx/03.open_property_pane.png)
    
3. Configure the web part according to the settings described in the **[Web Part Properties](./general.md)**;

4. The properties are saved automatically, so when you're done, simply **Publish** the page and the content will be saved.