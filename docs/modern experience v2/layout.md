Choose one of the 4 available layouts

![layout_options](../images/modernx/13.layout_options.png)

___
### Layout size
Choose between 2 sizes for the layout.

![layout_size](../images/modernx/14.layout_size.png)

<p class="alert alert-info">
Only available on tabular list
</p>

___
### Configure layout
Here you can configure the layout, by giving it new label names or new fields for each of the available positions.

![configure_layout](../images/modernx/15.configure_layout.png)

___
### Card color

You can choose a background color for the card

![palette_button](../images/modernx/16.palette_button.png) | ![free_picker_button](../images/modernx/17.free_picker_button.png)
------------- | -------------
![color_picker](../images/modernx/18.color_picker.png)  | ![color_picker2](../images/modernx/19.color_picker2.png)

The options are:

- **BindTuning Theme** - Choose a color from the BindTuning Theme palette, if a theme is applied.
- **Change the look** - Choose a color from the Change the look palette.
- **Free picker** - Choose any other color.

<p class="alert alert-info">
All selected colors will result in a lighter color, since it will automatically set an opacity of 15%
</p>

___
### Folder Structure
Select if you want the webpart to show the folder structure of your library.

___
### Folder Treeview
Select if you want the webpart to show a tree view of the folders on your library.

___
### Category Settings

You can configure the columns to show with a different color if a field matches the given value.

![category_settings](../images/modernx/20.category_settings.png)
