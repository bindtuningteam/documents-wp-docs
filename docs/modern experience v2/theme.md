![theme](../images/modernx/38.theme.png)

- **Apply theme color on title** - Displays the web part title with the theme's primary color on background.
- **Apply theme fonts** - Inherit the fonts of a BindTuning theme, if exists.