### Allow users to upload
This allows users to add their documents through the webpart.

___
### Allow users to edit files
This allows users to edit documents through the webpart.

___
### Notify me
This allows to add a new alert or manage past alerts about modifcations done to the select document library.

![alerts](../images/modernx/27.alerts.png)
