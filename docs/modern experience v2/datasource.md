![data_source](../images/modernx/05.datasource.png)


Decide where the data will be stored. Inside the web part or on a SharePoint List:
 
- **Get data from list** - Connect the web part to a SharePoint lists and gets data from it.
- **SharePoint Search** - Gets all documents from within SharePoint.

</br>

___
### Get data from list

![site_origin](../images/modernx/06.site_origin.png)


- **Current site** - Choose or create a list on the same collection.
- **Other sites** - Choose or create a list on any site collection.

</br>

#### Other sites
Search for a site or pick from frequent or recent sites

![site_picker](../images/modernx/07.choose_site.png)

</br>

Choose a list action

![list_Actions](../images/modernx/08.list_actions.png)

</br>

- **Create new** - Create a list on the selected site by entering a list name and click "Create list" button. After the list has been created, the web part will automatically be connected to that list.

![create_list](../images/modernx/09.create_list.png)

- **Existing list** - Search for an existing list.

![create_list](../images/modernx/10.existing_list.png)

</br>

___
### Choose a view or filter with a caml query

Click on the cog weel

![create_list](../images/modernx/11.configure_mappings_button.png)


</br>
To filter the view, you have two options:

![create_list](../images/modernx/12.select_view.png)

</br>

- **Views** - Choose a view from available views.

- **Caml Query** - Apply a caml query.

#### Caml Query

You query must have the following structure

``` <View><Query><Where></Where></Query></View> ```

<p class="alert alert-info">
You can create caml queries using U2U Caml Query builder tool. Download it, <a href="https://www.u2u.be/software" target="blank">here</a>
</p>

</br>

#### Target Folder

You can also choose a target folder so only the items within that folder are displayed.
