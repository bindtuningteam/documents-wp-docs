You can change the date format of the webpart by choosing one of the following options.

![format](../images/modernx/21.format.png)
