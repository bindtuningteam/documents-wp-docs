![Add section](../images/modern/03.advancedoptions.png)
___
### File Label Source 

Here you can select which column from your document library you want to use as a file's label. You should use the column's internal name here.

Here is what you need to do to check a column's **internal name**: 
1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...&Field=..."**.  
5. Copy the internal name;
6. Now paste the name on the text box;

![internalcolumnname](../images/classic/31.internalcolumnname.png) 

___
### Open Document With

Decide what happens when a user clicks on a document:

**Preview Modal** - Opens a modal on the page with a document preview, if available.

**Browser** - Opens the document on a new tab to be previewed. If the browser can't preview the file, it will download it instead.

**Direct Download** - The file will be downloaded whenever you click on it.

___
### Allow Edit Documents

When enabled, allows the editing of office documents directly from the preview modal.

___
### Allow Users to upload Files

When enabled, users can upload files into the library by dragging and droping files into the web part. 
This option is only available if the Folder Structure is also enabled.

___
### Show Upload Button

When enabled, an upload button is drawn above the web part content that users can use to upload files into the library. 
This option is only available if the Allow users to upload files is enabled

___
### Always show BT Buttons

When this is **turned on** the icons to manage the content of the Web Part are always visible when the page is in edit mode.<br>
When the feature is **turned off** you need to hover the web part to see the controls

___
### Title Link 

Note this option is only valid if you have a Title on the Web Part.<br>
On the **Title** of the Web Part, you can add an URL to redirect you to any page. The Web Part will always open the new URL in a new tab of your browser and that option is only visible if you're not in **Edit** mode.

___
### Title

If you click on the Title of the Web Part on the page, you can change the Title to the Web Part that will be shown. When you insert a text there it'll show on the page the **Title**, otherwise, nothing will be visible in the page.
