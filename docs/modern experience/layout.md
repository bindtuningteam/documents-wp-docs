![Add section](../images/modern/14.layout.png)

___
### Folder Structure

Enable this option if you want to organize your documents into folders
This option is not available when using Search Results or Target Folder options.

___
### Image Thumbnails

If you enable this option, images will be represented by a thumbnail instead of the default image icon

___
### Layout Template

Documents can be displayed in different ways, by selecting different layouts. 3 layouts are available:

|**Tiles**|**List View 1**|**List View 2**|
|-|-|-|
|![tiles_layout.png](../images/classic/34.tiles_layout.png)|![list_view1.png](../images/classic/35.list_view1.png)|![list_view2.png](../images/classic/36.list_view2.png)|

___
### Number of Columns

For the List View 1 and List View 2 layouts, this option allows to select a maximum number of columns for your items to take. The desired number of columns will only be used if the space available permits.

___
### Rows per Page

Use this option to decide how many rows of items you want to see per page. 
You can use this option to decide how much vertical space you want your web part to take.