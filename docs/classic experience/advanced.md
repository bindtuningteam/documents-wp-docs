![insert-tab](../images/classic/12.advanced.png)

### File Label Source

Here you can select which column from your document library you want to use as a file's label. You should use the column's internal name here.

Here is what you need to do to check a column's **internal name**: 
1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...&Field=..."**.  
5. Copy the internal name;
6. Now paste the name on the text box;

![internalcolumnname](../images/classic/31.internalcolumnname.png) 

___
### Open Document With

Decide what happens when a user clicks on a document:

**Preview Modal** - Opens a modal on the page with a document preview, if available.

**Browser** - Opens the document on a new tab to be previewed. If the browser can't preview the file, it will download it instead.

**Direct Download** - The file will be downloaded whenever you click on it.

___
### Allow Users to upload Files

When enabled, users can upload files into the library by dragging and droping files into the web part. 
This option is only available if the Folder Structure is also enabled.

___
### Show Upload Button

When enabled, an upload button is drawn above the web part content that users can use to upload files into the library. 
This option is only available if the Allow users to upload files is enabled
___
### Allow Edit Documents

When enabled, allows the editing of office documents directly from the preview modal.

___
### Language

From here you can select which language you want to be displayed by the web part. This will also translate the forms, but a refresh may be required before you see the changes.

By default, the BindTuning Web Part provides the language for:

- English
- Portuguese

If you'd like to localize the web part to your own language, please refer to [this article](https://support.bindtuning.com/hc/en-us/articles/115004585263).