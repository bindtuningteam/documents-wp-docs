![generaloptions](../images/classic/32.item_sorting.png)

Here you can define what will be the order of your tiles using the internal name of one of your list colunms name.

You will need to type the **internal name** of the list column on the Order by text box - we will sort the documents according to the value you enter.




Here is what you need to do to check a column's **internal name**: 
1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...&Field=..."**.  
5. Copy the internal name;
6. Now paste the name on the text box;

![internalcolumnname](../images/classic/31.internalcolumnname.png) 


