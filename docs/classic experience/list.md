### Create a Document Library

The first step of your web part configuration is creating a SharePoint Document Library. You will use this list to store your documents.

Let's create your list:

1. Open the settings menu and click on **Add an app**;

	![addanapp](../images/classic/17.addanapp.png)

2. On the search box type **Document**;
3. Look for the **Document Library** and open it;
4. Pick a name for the app and click on **Create**;
5. You've created the list that will contain all the specific fields from this web part.

![addanapp](../images/classic/24.calendar_list.png)

_____
For your web part to work, you need to choose between using the **SharePoint Document Library** list that you created or to use the **Search Results** option, that works with <a href="https://dev.office.com/sharepoint/docs/general-development/search-in-sharepoint">SharePoint Search</a>.

![event_source](../images/classic/02.event_source.gif)

___
### SharePoint Document Libraries

Now is time to select the list that you created. In the first field, you can write your SharePoint site relative path or the list URL. In the second one, you should select the list that you want to use in this web part and click on the disk icon to save it. 

![calendar_events](../images/classic/22.calendar_events.png)

<p class="alert alert-warning">Use relative paths for this field. So instead of using an URL like <strong>https://company.sharepoint.com/sites/Home</strong>, you should use something like <strong>/sites/Home/</strong>.</p>

___
### Search Results

![serachresults.png](../images/classic/23.calendar_search.png)

By choosing this option, you'll be able to:

1. **Search all of SharePoint**, if you want to search all the documents in all site collections (to wich you have access);  
2. **Search a site collection** if you want to search all the documents in a specific site collection.  Here you need to write the **Site Collection URL** in a field below the dropdown.

<p class="alert alert-warning">Note that the <strong>Folder Structure</strong> option will not work while using search.</p>