![options](../images/modern/07.options.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Documents Source](./list)
- [Filter Settings](./filters)
- [Layout Options](./layout)
- [Web Part Appearance](./appearance)
- [Advanced Options](./advanced)
- [Web Part Messages](./message)
- [Performance](./performance)