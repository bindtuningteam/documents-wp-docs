pillow==10.0.0
mock==1.0.1
alabaster==0.7.1
commonmark==0.9.1
recommonmark==0.5.0
mkdocs==1.5.1
docutils==0.20.1
    # via recommonmark==0.5.0
sphinx==7.0.1
    # via recommonmark==0.5.0
click==8.1.3
    # via mkdocs
ghp-import==2.1.0
    # via mkdocs
jinja2==3.1.2
    # via mkdocs
markdown==3.3.7
    # via mkdocs
mergedeep==1.3.4 
    # via mkdocs
packaging==23.1 
    # via mkdocs
pyyaml-env-tag==0.1 
    # via mkdocs
pyyaml==6. 
    # via mkdocs
watchdog==3.0.0
    # via mkdocs
python-dateutil==2.8.2
    # via ghp-import==2.1.0->mkdocs
MarkupSafe==2.1.3
    # via jinja2>=2.11.1->mkdocs
sphinxcontrib-applehelp==1.0.4
    # via sphinx>=1.3.1->recommonmark==0.5.0
sphinxcontrib-devhelp==1.0.2
    # via sphinx>=1.3.1->recommonmark==0.5.0
sphinxcontrib-jsmath==1.0.1
    # via sphinx>=1.3.1->recommonmark==0.5.0
sphinxcontrib-htmlhelp==2.0.1
    # via sphinx>=1.3.1->recommonmark==0.5.0
sphinxcontrib-serializinghtml==1.1.5
    # via sphinx>=1.3.1->recommonmark==0.5.0
sphinxcontrib-qthelp==1.0.3
    # via sphinx>=1.3.1->recommonmark==0.5.0
Pygments==2.15.1
    # via sphinx>=1.3.1->recommonmark==0.5.0
snowballstemmer==2.2.0
    # via sphinx>=1.3.1->recommonmark==0.5.0
babel==2.12.1
    # via sphinx>=1.3.1->recommonmark==0.5.0
imagesize==1.4.1
    # via sphinx>=1.3.1->recommonmark==0.5.0
requests==2.31.0
    # via sphinx>=1.3.1->recommonmark==0.5.0
six==1.16.0
    # via python-dateutil>=2.8.1->ghp-import>=1.0->mkdocs
charset-normalizer==3.1.0
    # via requests>=2.25.0->sphinx>=1.3.1->recommonmark==0.5.0
idna==3.4
    # via requests>=2.25.0->sphinx>=1.3.1->recommonmark==0.5.0
urllib3==2.0.3
    # via requests>=2.25.0->sphinx>=1.3.1->recommonmark==0.5.0
certifi==2023.5.7
    # via requests>=2.25.0->sphinx>=1.3.1->recommonmark==0.5.0